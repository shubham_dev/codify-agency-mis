const utilities = require('../utilities')
const parse = require('./parser')
const route = require('./router')
const auth = require('./auth')
const mapper = require('./mapper')()
module.exports = function (app, config) {
    parse(app, utilities.config)
    route({ "app": app, "config": config, "mapper": mapper })
    auth(app, config)
    route({ "app": app, "config": config, "mapper": mapper, "auth": true })
}
