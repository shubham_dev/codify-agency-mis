module.exports = (options) => {
    try {
        if (options.auth) {
            options.app.use(options.mapper.secured.routes());
            options.app.use(async (ctx, next) => {
                if (options.mapper.secured.match(ctx.path, ctx.method).pathAndMethod.length) {
                    return;
                } else {
                    ctx.status = 403;
                    ctx.body = {
                        err: "Unauthorized request"
                    };
                    return;
                }
                await next();
            });
        } else {
            options.app.use(options.mapper.public.routes());
            options.app.use(async (ctx, next) => {
                if (options.mapper.public.match(ctx.path, ctx.method).pathAndMethod.length) {
                    return;
                }
                await next();
            });
        }
    } catch (err) {
        console.error("Error in routing request : ", err);
    }
};
