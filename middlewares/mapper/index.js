const fs = require("fs");
let router = require('koa-router'), publicAPI = router(), securedAPI = router();
module.exports = () => {
    fs.readdirSync('./controllers').forEach(file => {
        let controller = require('../../controllers/' + file);
        if (controller.publicRoute) {
            controller.publicRoute(publicAPI);
        }
        if (controller.securedRoute) {
            controller.securedRoute(securedAPI);
        }
    });
    return { "public": publicAPI, "secured": securedAPI };
};
