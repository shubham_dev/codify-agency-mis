let parser = require('koa-better-body');
let IncomingForm = require('formidable');
let convert = require('koa-convert');
module.exports = (app, config) => {
    let form = new IncomingForm();
    form.uploadDir = config.systemConfig.app.uploadDir;
    try {
        app.use(convert(parser({
            multipart: true,
            IncomingForm: form,
            textLimit: '50mb',
            formLimit: '50mb',
            urlencodedLimit: '50mb',
            jsonLimit: '50mb',
            bufferLimit: '50mb'
        })));
    }
    catch (err) {
        console.error("Error in parsing : ", err);
    }
};
