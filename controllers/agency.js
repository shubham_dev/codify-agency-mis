const Agency = require('../dbmodels/models/agency')
const Client = require('../dbmodels/models/client')


/**
 * @swagger
 * /v1/agency/clientinfo:
 *  get:
 *      description: Use this api to get the client with highest totalBilling
 *      responses: 
 *          '200' : 
 *              description: Highest valued client 
 */

 /**
  * @swagger
  * /v1/agency/create:
  *  post:
  *      description: Use this api to create agency and client
  *      parameters:
  *        - in: body
  *          name: "body"
  *          description: "Agent details"
  *          required: true
  *          schema:
  *             $ref: "#/definitions/agencyadd"
  *      responses: 
  *          '200' : 
  *              description: in this response you will get the agency id and total clients added 
  * definitions:
  *     agencyadd:
  *         type: ""
  *         required:
  *             - "agencyData"
  *             - "clientDataArr"
  *         properties: 
  *             agencyData:
  *                 $ref: "#/definitions/agency" 
  *             clientDataArr:
  *                 type: "array"
  *                 items:
  *                     $ref: "#/definitions/client"
  *     agency: 
  *         type: "object"
  *         required:
  *             - "address1"
  *             - "state"
  *             - "city"
  *             - "phone_number"
  *             - "name"         
  *         properties:
  *             address1:
  *                 type: "string"
  *                 example: "udyog vihar"
  *             address2:
  *                 type: "string"
  *                 example: "sector 30"
  *             state:
  *                 type: "string"
  *                 example: "Haryana"
  *             city:
  *                 type: "string"
  *                 example: "gurgaon"
  *             phone_number:
  *                 type: "string"
  *                 example: "7017879883"
  *             name:
  *                 type: "string"
  *                 example: "agency 1"
  *     client: 
  *         type: "object"
  *         required:
  *             - "email"
  *             - "name"
  *             - "totalBill"
  *             - "phone_number"     
  *         properties:
  *             email:
  *                 type: "string"
  *                 example: "shubham506ad@gmail.com"
  *             name:
  *                 type: "string"
  *                 example: "shubham jain"
  *             totalBill:
  *                 type: "number"
  *                 example: 999.9
  *             phone_number:
  *                 type: "string"
  *                 example: "9628471399"
  */


exports.securedRoute = (app) => {
    // here add all the secured routes
}

exports.publicRoute = (app) => {
    app.get('/v1/agency/clientinfo', getClientInfo) //?agencyid=

    app.post('/v1/agency/create', createAgencyWithCLients)
}

const getClientInfo = async (ctx) => {
    try {
        const clientData = await Client.find({}, {
                name: 1,
                agencyId: 1,
                totalBill : 1
            }).populate({
                path: 'agencyId',
                model: 'Agency',
                select: "name"
            })
            .sort({
                'totalBill': -1
            }).limit(1)
        ctx.body = clientData[0]
        ctx.status = 200
    } catch (err) {
        ctx.body = err.message
        ctx.status = err.status || 500
    }
}

const createAgencyWithCLients = async (ctx) => {
    try {
        const body = ctx.request.fields

        if (!body.agencyData) {
            throw new Error('No Agency is present')
        }

        const agency = await new Agency(body.agencyData).save()

        const {clientErrorArr, clientDataArr} = parseClientData(body.clientDataArr, agency._id)
        const Bulk = Client.collection.initializeUnorderedBulkOp();
        for (let clientNum = 0; clientNum < clientDataArr.length; clientNum++) {
            Bulk.insert(new Client(clientDataArr[clientNum]));
        }

        const data = await new Promise((r, j) => {
            Bulk.execute((e, d) => { if (e) j(e); r(d); });
        }).then((d) => {
            return d;
        }).catch((e) => {
            throw e;
        });

        ctx.body = {
            "msg": "clients added successfully",
            total: data.nInserted,
            agencyId: agency._id
        }
        if (clientErrorArr.length > 0) {
            ctx.body['errored'] = clientErrorArr
            ctx.status = 206
        } else {
            this.status = 200;
        }

    } catch (err) {
        ctx.body = err.message
        ctx.status = err.status || 500
    }
} 

const parseClientData = (clientArr, agencyId) => {
    let clientErrorArr = []
    let clientDataArr = []

    clientArr.forEach((client, idx) => {
        let clientObjErr = []
        if (!client.name) {
            clientObjErr.push(`Client ${idx + 1} name is not present`)
        }

        if (!client.email) {
            clientObjErr.push(`Client ${idx + 1} email is not present`)
        }
        if (!client.phone_number) {
            clientObjErr.push(`Client ${idx + 1} phone number is not present`)
        }

        if (clientObjErr.length > 0) {
            clientErrorArr.push(clientObjErr)
        } else {
            client['agencyId'] = agencyId
            clientDataArr.push(client)
        }
    })
    return {clientErrorArr, clientDataArr}
}
