const Client = require('../dbmodels/models/client');

 /**
  * @swagger
  * /v1/udpdate/client/{clientid}:
  *  patch:
  *      description: Use this api to update client details
  *      parameters:
  *        - in: path
  *          name: clientid
  *          descrition: "client Id for updating the data"
  *          required: true
  *          type: "string"
  *        - in: body
  *          name: "body"
  *          description: "update client details"
  *          required: true
  *          schema:
  *             $ref: "#/definitions/client"
  *      responses: 
  *          '200' : 
  *              description: client details updated
  *          '406': 
  *              descripttion: wrong input details
  */


exports.securedRoute = (app) => {
  // here add all the secured routes
};

exports.publicRoute = (app) => {
  app.patch('/v1/udpdate/client/:clientid', updateClient)
}

const updateClient = async (ctx) => {
  try {
    const clientid = ctx.params.clientid
    const clientUpdates = ctx.request.fields
    const updatedData = await Client.findOneAndUpdate({_id: clientid}, {"$set" : clientUpdates}, {new : true}) 
    ctx.body = updatedData
    ctx.status = 200
  } catch (err) {
    ctx.status = 406
    ctx.body = err
  }
}