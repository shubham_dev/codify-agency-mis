const mongoose = require("mongoose")
const validator = require('validator')
var Float = require('mongoose-float').loadType(mongoose);
const Schema = mongoose.Schema

const ClientSchema = new Schema({
  email: {
    type: String,
      required: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw Error('Invalid Email Please enter correct email');
        }
      }
  },
  agencyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Agency'
  },
  phone_number: {
    type: String,
    required: true,
    trim: true,

  },
  name: {
    type: String,
    required: true,
    trim: true,
  },
  totalBill: {
    type: Float,
    required: true
  },
  created: {
    type: Date,
    required: false,
    default: Date.now,
  },
  modified: {
    type: Date,
    required: false,
    default: Date.now,
  },
})
mongoose.model('Client', ClientSchema)

ClientSchema.methods.toJSON = function () {
  const client = this
  const clientObject = client.toObject()
  clientObject['clientId'] = clientObject['_id']
  console.log('\n\n\n\n\ngettign the data', clientObject)
  	// agencyObject['totalBill'] = agencyObject['totalBill']['$numberDecimal']


  delete clientObject._id
  return clientObject
}

ClientSchema.index({ "totalBill": -1 });
const Client = mongoose.model('Client', ClientSchema)
module.exports = Client