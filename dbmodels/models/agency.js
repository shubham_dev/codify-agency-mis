const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AgencySchema = new Schema({
	address1: {
		type: String,
		required: true,
		trim: true,
		
	}, 
	address2: {
		type: String,
		trim: true,

	}, 
	state: {
		type: String,
		required: true,
		trim: true,

	}, 
	city: {
		type: String,
		required: true,
		trim: true,

	}, 
	phone_number: {
		type: String,
		required: true,
		trim: true,

	},
	name: {
		type: String,
		required: true,
		trim: true,
	},
	created: {
		type: Date,
		required: false,
		default: Date.now,
	},
	modified: {
		type: Date,
		required: false,
		default: Date.now,
	},
})

AgencySchema.virtual('clients', {
	ref: 'Client',
	localField: '_id',
	foreignField: 'owner'
})

AgencySchema.methods.toJSON = function () {
	const agency = this
	const agencyObject = agency.toObject()
	agencyObject['agencyId'] = agencyObject['_id']

	delete agencyObject._id
	return agencyObject
}

AgencySchema.pre('save', async function (next) {
	const agency = this
	let errorList = []

	if (!agency.name) {
		errorList.push('Agency name is not present')
	}

	if (!agency.address1) {
		errorList.push('Agency address1 is not present')
	}
	if (!agency.state) {
		errorList.push('Agency state is not present')
	}
	if (!agency.city) {
		errorList.push('Agency city is not present')
	}
	if (!agency["phone_number"]) {
		errorList.push('Agency phone number is not present')
	}

	if (errorList.length > 0) {
		throw new Error(errorList)
	}
	next()
})


const Agency = mongoose.model('Agency', AgencySchema)
module.exports = Agency
