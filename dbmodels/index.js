'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.initDatabase = (utilities) => {
    let connectUrl = utilities.config.systemConfig.mongo.getURI();
    let dbPromise = new Promise(function (resolve, reject) {
        mongoose.connection.on('disconnected', function (error) {
            console.log('cannot connect');
            reject(error);
        });
        mongoose.connection.on('connected', function () {
            console.log('succesfully connected to the database');
            resolve(true);
        });
    });
    mongoose.set('useCreateIndex', true);
    mongoose.Promise = global.Promise;
    mongoose.connect(connectUrl, { useUnifiedTopology: true, useNewUrlParser: true });
    return dbPromise;
};
