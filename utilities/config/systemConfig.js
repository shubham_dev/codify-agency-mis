const path = require('path')
const systemConfig = {
    "dev": {
        "app": {
            "host": 'localhost',
            "port": process.env.PORT || '3000',
            "downloadDir": path.join(__dirname + '/../../downloads'),
            "cookieOptions": {
                "domain": `http://${this.host}/${this.port}`
            }
        },
        "mongo": {
            seed: false,
            debug: true,
            dbname: 'codify-agency-mis',
            host: '127.0.0.1',
            port: 27017,
            getURI: function (exFlag) {
                return 'mongodb+srv://codify-admin:codify@cluster0-jdsim.mongodb.net/test?retryWrites=true&w=majority'
            }
        },
        "log": {
            level: 'debug',
            path: __dirname + '/../../logs/msgapp.log'
        }
    }
}
module.exports = systemConfig['dev']